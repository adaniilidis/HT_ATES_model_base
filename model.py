#import darts.engines

from darts.models.physics.geothermal_operators import *
from darts.models.physics.geothermal import Geothermal
from darts.models.darts_model import DartsModel
from darts.models.reservoirs.struct_reservoir import StructReservoir
# from MyGeothermal import MyGeothermal
import numpy as np
import os
import csv
import sys


from darts.engines import value_vector
#from geothermal import Geothermal


class Model(DartsModel):
    def __init__(self, poro, perm,
                 set_newton_tolerance,
                 set_linear_tolerance,
                 set_nx, set_ny, set_nz,
                 set_dx, set_dy, set_dz,
                 w0_x, w0_y, w1_x, w1_y,
                 set_nperf, set_perfstart, set_topdomain,
                 set_cond, set_hcap,
                 set_platform):
        # call base class constructor
        super().__init__()

        self.timer.node["initialization"].start()

        self.nx = set_nx
        self.ny = set_ny
        self.nz = set_nz
        self.nb = self.nx * self.ny * self.nz
        self.dx = np.ones(self.nb)*set_dx
        self.dy = np.ones(self.nb)*set_dy
        # self.dx = np.ones(self.nb)* 40
        # self.dy = np.ones(self.nb)* 40
        self.dz = np.ones(self.nb)*set_dz
        # self.dz = np.ones(self.nb) * 0.2
        set_depth = set_topdomain - set_dz + self.dz.reshape(self.nx,self.ny,self.nz).cumsum(axis=2)
        self.depth = set_depth.flatten(order='F')
        self.midrespress = self.depth[int(len(self.depth)/2)] / 1000 * 100 + 1
        self.bhp_limit = self.midrespress * 0.8


        # self.poro = np.array(poro) * np.ones(self.nb)
        # self.kx = np.array(perm) * np.ones(self.nb)
        self.poro = poro
        self.kx = perm
        self.ky = self.kx
        self.kz = 0.1 * self.kx

        self.actnum = np.ones((self.nx, self.ny, self.nz))

        # boundaries will be applied to those blocks which remained active!
        self.reservoir = StructReservoir(self.timer, nx=self.nx, ny=self.ny, nz=self.nz, dx=self.dx, dy=self.dy,
                                         dz=self.dz, permx=self.kx, permy=self.ky, permz=self.kz,
                                         poro=self.poro, depth=self.depth, actnum=self.actnum)

        # get wrapper around local array (length equal to active blocks number)
        cond_mesh = np.array(self.reservoir.mesh.rock_cond, copy=False)
        hcap_mesh = np.array(self.reservoir.mesh.heat_capacity, copy=False)

        cond_mesh[:] = set_cond # hcond = kJ/m/day/K
        hcap_mesh[:] = set_hcap # units: hcap = kJ/m3/K
        # cond_mesh.fill(100) # hcond = kJ/m/day/K
        # hcap_mesh.fill(2200) # units: hcap = kJ/m3/K
        #init_cond = set_init_cond
        # assign values from global array, filtered by actnum, to the wrapper
        # cond_mesh[:] = cond_mesh[self.reservoir.discretizer.global_to_local > -1]

        # self.reservoir.set_boundary_volume(xy_minus=1e10, xy_plus=1e10, xz_minus=1e10, xz_plus=1e10)
        self.reservoir.set_boundary_volume(xz_minus=1e10, xz_plus=1e10)

        self.reservoir.add_well("HotWell")
        n_perf = set_nperf
        for n in range(n_perf):
            self.reservoir.add_perforation(well=self.reservoir.wells[-1], i=w0_x, j=w0_y, k=n+set_perfstart,
                                           multi_segment=False, verbose=True)
        self.reservoir.add_well("ColdWell")
        # n_perf = 27
        for n in range(n_perf):
            self.reservoir.add_perforation(well=self.reservoir.wells[-1], i=w1_x, j=w1_y, k=n+set_perfstart,
                                           multi_segment=False, verbose=True)

        #self.physics = Geothermal(self.timer, 16, 1, 500, 1, 6000)
        self.physics = Geothermal(self.timer, 16, 1e-3, 600, 1, 15000, platform=set_platform, grav=True)

        self.params.first_ts = 0.001
        self.params.mult_ts = 8
        self.params.max_ts = 365

        # Newton tolerance is relatively high because of L2-norm for residual and well segments
        self.params.tolerance_newton = set_newton_tolerance
        self.params.tolerance_linear = set_linear_tolerance
        self.params.max_i_newton = 20
        self.params.max_i_linear = 40

        # self.params.newton_type = 1
        self.params.newton_params = value_vector([1])

        self.runtime = 3
        # self.physics.engine.silent_mode = 0
        self.timer.node["initialization"].stop()

    def set_tolerance_newton(self, set_newton_tolerance):
        self.params.tolerance_newton = set_newton_tolerance


    def set_initial_conditions(self):
        self.timer.node["initialization"].start()
        self.timer.node["initialization"].node["initial conditions"] = timer_node()
        self.timer.node["initialization"].node["initial conditions"].start()


        self.physics.set_nonuniform_initial_conditions(self.reservoir.mesh, 100, 30)


        self.timer.node["initialization"].node["initial conditions"].stop()
        self.timer.node["initialization"].stop()


    def set_boundary_conditions(self):
        for i, w in enumerate(self.reservoir.wells):
            if i <= 0:
                w.control = self.physics.new_rate_water_inj(3600, 284.15)
                w.constraint = self.physics.new_bhp_water_inj(490, 284.15)
            else:
                w.control = self.physics.new_rate_water_prod(3600)
                w.constraint = self.physics.new_bhp_prod(10)

    def plot_temp_layer_map(self, map_data, k, name):
        import plotly
        import plotly.graph_objs as go
        import numpy as np
        from darts.models.physics.iapws.iapws_property import iapws_temperature_evaluator
        nxny = self.reservoir.nx * self.reservoir.ny

        temperature = iapws_temperature_evaluator()
        used_data = map_data[2 * nxny * (k - 1): 2 * nxny * k]
        T = np.zeros(nxny)
        for i in range(0, nxny):
            T[i] = temperature.evaluate([used_data[2 * i], used_data[2 * i + 1]])

        data = [go.Heatmap(
            z=T.reshape(self.reservoir.ny, self.reservoir.nx))]  # .transpose()
        layout = go.Layout(title='%s, layer %d' % (name, k),
                           yaxis=dict(scaleratio=1, scaleanchor='x', title='X, block'),
                           xaxis=dict(title='Y, block'))
        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename='%s_%d_map.html' % (name, k))

    def plot_pres_layer_map(self, map_data, k, name):
        import plotly
        import plotly.graph_objs as go

        nxny = self.reservoir.nx * self.reservoir.ny
        data = [go.Heatmap(
            z=map_data[nxny * (k - 1): nxny * k].reshape(self.reservoir.ny, self.reservoir.nx))]  # .transpose())]
        layout = go.Layout(title='%s, layer %d' % (name, k),
                           yaxis=dict(scaleratio=1, scaleanchor='x', title='X, block'),
                           xaxis=dict(title='Y, block'))
        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename='%s_%d_map.html' % (name, k))

    def plot_layer_surface(self, map_data, k, name):
        import plotly
        import plotly.graph_objs as go

        nxny = self.reservoir.nx * self.reservoir.ny
        map_data = map_data[nxny * (k - 1): nxny * k].reshape(self.reservoir.ny, self.reservoir.nx)  # .transpose()
        data = [go.Surface(z=map_data)]
        plotly.offline.plot(data, filename='%s_%d_surf.html' % (name, k))

    def set_rate(self, rate, welln, temp=300):
        w = self.reservoir.wells[welln]
        if welln == 0:
            w.control = self.physics.new_rate_water_inj(rate, temp)
        else:
            w.control = self.physics.new_rate_water_prod(rate)

    def set_rate_hot(self, rate, welln=0, temp=300, func='inj'):
        w = self.reservoir.wells[welln]
        if func == 'inj':
            w.control = self.physics.new_rate_water_inj(rate, temp)
            w.constraint = self.physics.new_bhp_water_inj(self.midrespress + self.bhp_limit , temp)
        if func == 'prod':
            w.control = self.physics.new_rate_water_prod(rate)
            w.constraint = self.physics.new_bhp_prod(self.midrespress - self.bhp_limit)

    def set_rate_cold(self, rate, welln=1, temp=300, func='inj'):
        w = self.reservoir.wells[welln]
        if func == 'inj':
            w.control = self.physics.new_rate_water_inj(rate, temp)
            w.constraint = self.physics.new_bhp_water_inj(self.midrespress + self.bhp_limit , temp)
        if func == 'prod':
            w.control = self.physics.new_rate_water_prod(rate)
            w.constraint = self.physics.new_bhp_prod(self.midrespress - self.bhp_limit)

    def set_yearly_well_controls(self, set_runtime = 1,
                                 set_rate = 5184,
                                 set_days_profile = [120, 60, 120, 65],
                                 set_InjT = 273.15 + 90,
                                 set_TCutOff = 273.15 + 50,
                                 set_well_profile = [1, 0, 1, 0],
                                 set_transition_runtime = 1e-5,
                                 set_reference_newton_tolerance = 1,
                                 set_transition_newton_tolerance= 1e-4):
        set_well_rates = [x * set_rate for x in set_well_profile]
        for i, runtime in enumerate(set_days_profile):
            if i == 0:
                self.set_rate_hot(set_well_rates[i], temp = set_InjT, func= 'inj')
                self.set_rate_cold(set_well_rates[i], func='prod')
                print('Charge')
            if i ==1:
                self.set_rate_hot(set_well_rates[i], temp = set_InjT, func= 'inj')
                self.set_rate_cold(set_well_rates[i], func='prod')
                print('Store')
            if i ==2:
                self.set_rate_hot(set_well_rates[i], func= 'prod')
                self.set_rate_cold(set_well_rates[i], temp=set_TCutOff, func='inj')
                print('Discharge')
            if i ==3:
                self.set_rate_hot(set_well_rates[i], temp = set_InjT, func= 'inj')
                self.set_rate_cold(set_well_rates[i], func='prod')
                print('Rest')

            print('Newton transition tolerance set to %s'%set_transition_newton_tolerance)
            self.set_tolerance_newton(1e-4)
            self.run_python(set_transition_runtime, restart_dt=set_transition_runtime*0.1)
            print('Newton tolerance back to reference: %s'%set_reference_newton_tolerance)
            self.set_tolerance_newton(set_newton_tolerance)
            self.run_python(runtime-set_transition_runtime, restart_dt=1e-4)

    def export_vtk_model(self, set_vtk_path, file_name='data', local_cell_data={}, global_cell_data={},
                         vars_data_dtype=np.float32,
                         export_grid_data=True):

        # get current engine time
        t = self.physics.engine.t
        nb = self.reservoir.mesh.n_res_blocks
        nv = self.physics.n_vars
        X = np.array(self.physics.engine.X, copy=False)

        for v in range(nv):
            local_cell_data[self.physics.vars[v]] = X[v:nb * nv:nv].astype(vars_data_dtype)

        self.reservoir.export_vtk_custom(set_vtk_path, file_name, t, local_cell_data, global_cell_data,
                                         export_grid_data)
    def export_pro_vtk(self, file_name='temperature'):
        from darts.models.physics.iapws.iapws_property_vec import _Backward1_T_Ph_vec
        nb = self.reservoir.nb
        T = np.zeros(nb)
        X = np.array(self.physics.engine.X)
        T[:] = _Backward1_T_Ph_vec(X[:nb * 2:2] / 10, X[1:nb * 2:2] / 18.015)
        self.export_vtk(file_name=file_name, local_cell_data={'temp': T})

    def assign_initial_condition(self, filename = None,
                                 depth_enthalpy_array = None):#mesh,pressure_grad,temperature_grad):

        if filename:
            print('Found existing depth - enthalpy data file: %s'%filename)
            depth_enthalpy_dict = dict(map(np.float32, np.loadtxt(filename, delimiter=',')))
        if depth_enthalpy_array:
            print('Computed unique depth - enthalpy data')
            depth_enthalpy_dict = dict(map(np.float32, depth_enthalpy_array))

        set_enthalpy = list(np.array(self.reservoir.mesh.depth, copy=True))
        set_enthalpy = np.array([depth_enthalpy_dict.get(k) for k in set_enthalpy])
        #print('set_enthalpy has shape: %s' % set_enthalpy.shape)
        if np.isnan(set_enthalpy).any():
            print(
                'Depth - Enthalpy data does not cover full reservoir depth range\nconsider recomputing depth-enthalpy or expanding range')
            exit()
        # enthalpy = np.array(map(depth_enthalpy_dict.get, enthalpy))
        print(set_enthalpy)
        enthalpy = np.array(self.reservoir.mesh.enthalpy, copy=False)
        # enthalpy = np.array(mesh.enthalpy, copy=False)
        enthalpy[:] = set_enthalpy