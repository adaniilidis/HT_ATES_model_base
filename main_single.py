from model import Model
from print_redirection_functions import *
import pandas as pd
import numpy as np
import os
from darts.engines import redirect_darts_output
# import discretize

xmin = 0
xmax = 500
xdim = xmax - xmin
ymin = 0
ymax = 500
ydim = ymax - ymin

resthick = 50
zbuffer = 50
topres = 600
zdim = (zbuffer*2) + resthick
set_dx = 20
set_dy = 20
set_dz = 10
set_topdomain = topres - zbuffer - set_dz

set_nx = int(xdim / set_dx)
set_ny = int(ydim / set_dy)
set_nz = int(zdim / set_dz) + 2
ncells = set_nx *set_ny * set_nz
set_perfstart = int(2 + (zbuffer/set_dz))
set_nperf = int(resthick/set_dz)
wspacing = 140
w0_x, w0_y = int(set_nx/2) - int(wspacing/2/set_dx) +1, int(set_ny/2)
w1_x, w1_y = int(set_nx/2) + int(wspacing/2/set_dx) +1, int(set_ny/2)
set_newton_tolerance = 1e-2
set_linear_tolerance = 1e-5
set_transition_runtime = 1e-3
set_rate = 5184
set_run_years = 20
set_daysprofile = [120, 60, 120, 65]
set_wellprofile = [1, 0, 1, 0]
set_rates = [x * set_rate for x in set_wellprofile]
set_InjT = 273.15 + 90
set_TCutOff = 273.15 + 50
set_permres = [304]
set_porores = 0.1
bound_poro, bound_perm = 0.01, 0.1

# try:
#     from darts.engines import set_num_threads
#     set_num_threads(1)
# except:
#     pass

for i,perm in enumerate(set_permres):
    filename = 'basemodel_tolerance_1e-2'
    res_poro = set_porores * np.ones(set_nx * set_ny * int(resthick / set_dz))
    res_perm = perm * np.ones(set_nx * set_ny * int(resthick / set_dz))
    bound_poro = bound_poro * np.ones(set_nx * set_ny * int((zbuffer + set_dz) / set_dz))
    bound_perm = bound_perm * np.ones(set_nx * set_ny * int((zbuffer + set_dz) / set_dz))
    hcap, hcond = 832, 242  # Matching here the values of Julian units: hcap = kJ/m3/K, hcond = kJ/m/day/K
    bound_hcap, bound_hcond = 860, 196.5

    set_poro = np.concatenate((bound_poro, res_poro, bound_poro), axis=0)
    set_perm = np.concatenate((bound_perm, res_perm, bound_perm), axis=0)

    set_hcap = set_poro.copy()
    np.where(set_hcap <= 0.01, bound_hcap,  hcap)
    set_cond = set_poro.copy()
    np.where(set_cond <= 0.01, bound_hcond, hcond)

    porotest = set_poro.reshape(set_nx, set_ny, set_nz, order='F')

    # tmesh = discretize.TensorMesh([set_nx, set_ny, set_nz])
    # vals = porotest
    # tmesh.plot_3d_slicer(vals)

    if len(set_poro) != ncells:
        print("Missmatch between number of cells and porosity values!")

    log_file = filename + '.log'

    log_stream = redirect_darts_output(log_file)

    m = Model(set_poro, set_perm,
              set_newton_tolerance, set_linear_tolerance,
              set_nx, set_ny, set_nz,
              set_dx, set_dy, set_dz,
              w0_x, w0_y, w1_x, w1_y,
              set_nperf, set_perfstart, set_topdomain,
              set_cond, set_hcap,
              set_platform='cpu')

    m.init()
    m.export_pro_vtk(file_name=filename)

    set_rate = 5184
    injprofile = [1, 0, 1, 0]
    prodprofile = [1, 0, 1, 0]
    hotrates = [x * set_rate for x in injprofile]
    coldrates = [x * set_rate for x in prodprofile]
    daysprofile = [120, 60, 120, 65]
    InjT = 273.15 + 90
    TCutOff = 273.15 + 50



    for k in range(set_run_years):
        # m.set_yearly_well_controls(set_runtime=k,
        #                          set_rate=5184,
        #                          set_days_profile=[120, 60, 120, 65],
        #                          set_InjT=273.15 + 90,
        #                          set_TCutOff=273.15 + 50,
        #                          set_well_profile=[1, 0, 1, 0],
        #                          set_transition_runtime=1e-5,
        #                          set_reference_newton_tolerance=1,
        #                          set_transition_newton_tolerance=1e-4)
        for i, runtime in enumerate(daysprofile):
            if i == 0:
                m.set_rate_hot(hotrates[i], temp = InjT, func= 'inj')
                m.set_rate_cold(coldrates[i], func='prod')
                print('Charge')
                # m.export_pro_vtk(file_name=filename)
            if i ==1:
                m.set_rate_hot(hotrates[i], temp = InjT, func= 'inj')
                m.set_rate_cold(coldrates[i], func='prod')
                print('Store')
                # m.export_pro_vtk(file_name=filename)
            if i ==2:
                m.set_rate_hot(hotrates[i], func= 'prod')
                m.set_rate_cold(coldrates[i], temp=TCutOff, func='inj')
                print('Discharge')
                # m.export_pro_vtk(file_name=filename)
            if i ==3:
                m.set_rate_hot(hotrates[i], temp = InjT, func= 'inj')
                m.set_rate_cold(coldrates[i], func='prod')
                print('Rest')
                # m.export_pro_vtk(file_name=filename)

            # print('Tolerance set to 1e-2')
            # m.set_tolerance_newton(1e-2)
            m.export_pro_vtk(file_name=filename)
            m.run_python(runtime, restart_dt=set_transition_runtime)

            # print('Tolerance set back to %s'%set_newton_tolerance)
            # m.set_tolerance_newton(set_newton_tolerance)
            # m.run_python(runtime - (set_transition_runtime*3*8))
            # m.run_python(runtime-set_transition_runtime)


    vtk_file = filename
    if os.path.exists(vtk_file):
        os.remove(vtk_file)

    #m.export_vtk_model(vtk_file)
    m.export_pro_vtk(file_name=filename)


    # m.print_timers()
    m.print_stat()
    m.print_timers()

    time_data = pd.DataFrame.from_dict(m.physics.engine.time_data)

    # identify and drop collumns that are not relevant for this study
    press_gridcells = time_data.filter(like='reservoir').columns.tolist()
    chem_cols = time_data.filter(like='Kmol/day').columns.tolist()
    time_data.drop(columns=press_gridcells + chem_cols, inplace=True)
    # add years as a collumn
    time_data['Time (years)'] = time_data.time / 365.25

    time_file = filename + '.xlsx'
    if os.path.exists(time_file):
        os.remove(time_file)

    writer = pd.ExcelWriter(time_file)
    time_data.to_excel(writer, 'Sheet1')
    writer.save()

